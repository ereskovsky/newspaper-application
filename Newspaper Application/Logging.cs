﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
namespace Newspaper_Application
{
    public class Logging
    {

        public static string logpath = "";
        public static void Log(string logs)
        {
            string curPath = System.Reflection.Assembly.GetExecutingAssembly().Location;
            string curDir = System.IO.Path.GetDirectoryName(curPath);
            logpath = System.IO.Path.Combine(curDir, "../../log.txt");
            
            using (var t = File.AppendText(logpath))
            {
                t.WriteLine(logs);
            }
        }
    }
}
