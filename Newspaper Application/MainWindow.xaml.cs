﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml.Serialization;
using System.Drawing;
using Microsoft.Win32;
using System.Security.Cryptography;
using System.Xml;

namespace Newspaper_Application
{
    
    public partial class MainWindow : Window
    {
        

        
        public MainWindow()
        {
            InitializeComponent();
            Logging.Log("Программа запущена: " + DateTime.Now);
            searchMode = false;
            var curPAth1 = System.Reflection.Assembly.GetExecutingAssembly().Location;
            var curDir = System.IO.Path.GetDirectoryName(curPAth1);
            
           
            var db = new TestBaseEntitiesNew();
            var elements = db.articles.ToList();
            int counter = 0;
            
            foreach (var article in elements)
            {
                StackPanel st = new StackPanel();
                TextBlock tbTitle = new TextBlock();
                TextBlock tbId = new TextBlock();
                tbTitle.Text = article.title;
                tbId.Text = "id: " + article.id.ToString();
                st.Name = "stackpanelArticle" + article.id.ToString();
                counter++;
                st.Children.Add(tbId);
                st.Children.Add(tbTitle);

                listBoxAdminListArticles.Items.Add(st);
            }
            db.Dispose();

            loadArticles(artPage);    

            
        }
       
        private int artPage = 0;
        private void loadArticles(int add=0 , List<articles> articlesInc=null)
        {
           
            using (var db2 = new TestBaseEntitiesNew())
            {
                List<articles> art9_all;
                if (articlesInc != null)
                {
                    art9_all = articlesInc.Take(9).ToList();
                }
                else
                {
                    art9_all = db2.articles.Take(9).ToList();
                }

                List<articles> art9 = new List<articles>();
                if (add > 0)
                {
                    int to = add + 9;
                    List<articles> art9_all2;
                    if (articlesInc == null)
                    {
                        if (to > db2.articles.Count())
                        {
                            to = db2.articles.Count();
                            artPage = artPage - 9;
                        }
                        art9_all2 = db2.articles.Take(to).ToList();
                    }
                    else
                    {
                        if (to > articlesInc.Count())
                        {
                            to = articlesInc.Count();
                            artPage = artPage - 9;
                        }
                        art9_all2 = articlesInc.Take(to).ToList();
                    }

                    for (int i = to-9; i < to; i++)
                    {
                        art9.Add(art9_all2[i]);
                    }
                }
                else
                {
                    art9 = art9_all;
                }

                var gridCh = gridArticle.Children;
                List<StackPanel> panels = new List<StackPanel>();
                foreach (var el in gridCh)
                {
                    if (typeof(StackPanel) == el.GetType())

                        panels.Add((StackPanel)el);
                }
                int counter2 = 0;
                foreach (var art in art9)
                {
                    StackPanel curr = panels[counter2];
                    Label lb = (Label)curr.Children[1];
                    System.Windows.Controls.Image img = (System.Windows.Controls.Image)curr.Children[0];
                    TextBox tx = (TextBox)curr.Children[2];
                    tx.Text = art.id.ToString();

                    var curPAth = System.Reflection.Assembly.GetExecutingAssembly().Location;
                    var path = art.picture;

                    path = path.Replace("/", "\\");
                    var filename = System.IO.Path.GetFileName(path);
                    var newPath =
                        System.IO.Path.Combine(System.IO.Path.GetDirectoryName(curPAth) + "/img/", filename);
                    BitmapImage newimg;
                    if (File.Exists(newPath))
                    {
                        newimg = new BitmapImage(new Uri(newPath));


                        img.Source = newimg;
                    }
                    else
                    {
                        var p404 = System.IO.Path.Combine(System.IO.Path.GetDirectoryName(curPAth) + "/img/", "404.jpg");
                        if (File.Exists(p404)) 
                        {
                            newimg = new BitmapImage(new Uri(p404));


                            img.Source = newimg;
                        }
                        else
                        {
                            img.Source = new BitmapImage();
                        }
                    }
                   
                    lb.Content = art.title;
                    counter2++;
                }
                
                for (int i = counter2; i < 9; i++)
                {
                    StackPanel curr = panels[i];
                    Label lb = (Label)curr.Children[1];
                    System.Windows.Controls.Image img = (System.Windows.Controls.Image)curr.Children[0];
                    TextBox tx = (TextBox)curr.Children[2];
                    lb.Content = "";
                    tx.Text = "";
                    img.Source = new BitmapImage();
                }

            }
        }
        
        private void buttonAdminAddArticle_Click(object sender, RoutedEventArgs e)
        {
            CurrentAdminArticleId = 0;
            
            textBoxAdminTitle.Text = "";
            textBoxSubTitle.Text = "";
            textBoxTopic.Text = "";
            textBoxAuthor.Text = "";
            imageAdminPicture.Source = null;
            string html = "<html><head>" +
                    "<meta http-equiv='Content-Type' content='text/html;charset=UTF-8'></head>"
                    + "<body contentEditable='true'> Введите текст статьи </body>";
            webBrowserArticle.NavigateToString(html);
        }

        private void buttonDeleteArticle_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (CurrentAdminArticleId != 0)
                {
                    using (var db = new TestBaseEntitiesNew())
                    {
                        db.articles.Remove(
                            db.articles.Where(art => art.id == CurrentAdminArticleId).FirstOrDefault());
                        db.SaveChanges();

                    }
                    int ind = listBoxAdminListArticles.SelectedIndex;
                    listBoxAdminListArticles.Items.RemoveAt(ind);
                    Logging.Log("Статья удалена: " + DateTime.Now);
                }
            }
            catch(Exception a)
            {
                MessageBox.Show(a.ToString());
                Logging.Log("ОШИБКА при удалении: " + a.ToString() + ": " + DateTime.Now);
            }
        }

        private void buttonSaveArticle_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                bool newarticle = false;
                if (CurrentAdminArticleId == 0)
                {

                    using (var db = new TestBaseEntitiesNew())
                    {
                        var newart = new articles();
                        newart.dateTime = DateTime.Now;
                        newart.picture = "";
                        newart.subtitle = "";
                        newart.text = "";
                        newart.title = "";
                        newart.topic = 0;
                        newart.author = 0;
                        var art = db.articles.Add(newart);
                        db.SaveChanges();
                        var id = art.id;
                        CurrentAdminArticleId = id;
                        newarticle = true;
                    }
                }


                var intid = CurrentAdminArticleId;
                using (var db = new TestBaseEntitiesNew())
                {
                    var article = db.articles.Where(art => art.id == intid).FirstOrDefault();
                    if (article != null)
                    {

                        dynamic doc = webBrowserArticle.Document;
                        string text2 = doc.documentElement.InnerHtml;
                        article.text = text2;
                        article.title = textBoxAdminTitle.Text;
                        article.subtitle = textBoxSubTitle.Text;

                        article.picture = CurrentAdminPicturePath;
                        article.author = CurAuthorId;
                        article.topic = CurTopicId;

                        if (newarticle)
                        {

                            StackPanel st = new StackPanel();
                            TextBlock tbTitle = new TextBlock();
                            TextBlock tbId = new TextBlock();
                            tbTitle.Text = article.title;
                            tbId.Text = "id: " + article.id.ToString();
                            st.Name = "stackpanelArticle" + article.id.ToString();

                            st.Children.Add(tbId);
                            st.Children.Add(tbTitle);
                            Logging.Log("Статья " + article.text + "добавлена" + ": " + DateTime.Now);
                            listBoxAdminListArticles.Items.Add(st);
                        }
                        else
                        {
                            StackPanel curStackPanel = (StackPanel)listBoxAdminListArticles.SelectedItem;

                            curStackPanel.Children.RemoveAt(1);
                            TextBlock tbTitle = new TextBlock();
                            tbTitle.Text = article.title;
                            curStackPanel.Children.Add(tbTitle);
                            Logging.Log("Статья " + article.text + "сохранена" + ": " + DateTime.Now);
                        }
                    }
                    db.SaveChanges();
                }
            }
            catch (Exception a)
            {
                MessageBox.Show(a.ToString());
                Logging.Log("ОШИБКА при сохранении: " + a.ToString() + ": " + DateTime.Now);
            }

        }

        private void buttonAdminLoadPicture_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                OpenFileDialog openFileDialog = new OpenFileDialog();
                openFileDialog.Filter = "jpeg|*.jpg";
                StackPanel curStackPanel = (StackPanel)listBoxAdminListArticles.SelectedItem;
                if (curStackPanel == null)
                {
                    return;
                }
                var name = curStackPanel.Name;
                var id = name.ToString().Replace("stackpanelArticle", "");
                var intid = int.Parse(id);
                if (openFileDialog.ShowDialog() == true)
                {
                    var db = new TestBaseEntitiesNew();
                    var article = db.articles.Where(art => art.id == intid).FirstOrDefault();
                    var fileN = openFileDialog.FileName;
                    Logging.Log("Картинка загружена - " + fileN + ": " + DateTime.Now);
                    var newPath = System.IO.Path.GetFileName(fileN);
                    var curPAth = System.Reflection.Assembly.GetExecutingAssembly().Location;
                    newPath = System.IO.Path.Combine(System.IO.Path.GetDirectoryName(curPAth) + "/img/", newPath);
                    article.picture = newPath;
                    db.SaveChanges();

                    if (!File.Exists(newPath))
                        File.Copy(fileN, newPath, true);

                    BitmapImage sharerimg;
                    sharerimg = new BitmapImage(new Uri(newPath));
                    imageAdminPicture.Source = sharerimg;
                    MessageBox.Show("Картинка сохранена");
                }
            }
            catch (Exception a)
            {
                MessageBox.Show(a.ToString());
                Logging.Log("ОШИБКА при загрузке картинки: " + a.ToString() + ": " + DateTime.Now);
            }
        }
        private string md5(string pass)
        {
            try
            {
                MD5 md5 = System.Security.Cryptography.MD5.Create();

                byte[] inputBytes = System.Text.Encoding.UTF8.GetBytes(pass);

                byte[] hash = md5.ComputeHash(inputBytes);


                StringBuilder sb = new StringBuilder();

                for (int i = 0; i < hash.Length; i++)

                {

                    sb.Append(hash[i].ToString("X2"));

                }

                return sb.ToString();
            }
            catch (Exception e)
            {
                string sb = "mistake";
                MessageBox.Show(e.ToString());
                return sb.ToString();
            }
        }
        
        private Auth auth;
        private void buttonOpenAdmin_Click(object sender, RoutedEventArgs e)
        {
            

            auth = new Auth();
            auth.Closed += Auth_Closed;
            auth.Show();

            
        }

        private void Auth_Closed(object sender, EventArgs e)
        {
            
            var db = new TestBaseEntitiesNew();
            var curlogin = auth.login;
            var curpass = auth.pass;
            if (curlogin == string.Empty)
                return;

            var md5s = md5(curpass);
            var user = db.authors.Where(a => a.login == curlogin).FirstOrDefault();

            Logging.Log("auth; user=" + curlogin + "; pass=" + curpass);

            if (user!=null && user.password.ToUpper() == md5s)
            {
                Logging.Log("Выполнен вход: " + DateTime.Now);
                gridAdmin.Visibility = Visibility.Visible;
                gridArticle.Visibility = Visibility.Collapsed;
                
                var elements = db.articles.ToList();
                int counter = 0;
                foreach (var article in elements)
                { 
                    StackPanel st = new StackPanel();
                    TextBlock tbTitle = new TextBlock();
                    TextBlock tbId = new TextBlock();
                    tbTitle.Text = article.title;
                    tbId.Text = "id: " + article.id.ToString();
                    st.Name = "stackpanelArticle" + article.id.ToString();
                    counter++;
                    st.Children.Add(tbId);
                    st.Children.Add(tbTitle);

                    listBoxAdminListArticles.Items.Add(st);
                }

            }
            else
            {
                
                MessageBox.Show("Неверный логин или пароль");
                Logging.Log("Неудачная попытка входа: " + DateTime.Now);
            }


            db.Dispose();
        }

        private decimal CurrentAdminArticleId = 0;
        private string CurrentAdminPicturePath = "";
        private int CurAuthorId = 0;
        private int CurTopicId = 0;
        private void listBoxAdminListArticles_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            StackPanel curStackPanel = (StackPanel)listBoxAdminListArticles.SelectedItem;
            if (curStackPanel == null)
            {
                return;
            }
            var name = curStackPanel.Name;
            var id = name.ToString().Replace("stackpanelArticle", "");
            var intid = int.Parse(id);
           
            using (var db = new TestBaseEntitiesNew())
            {
                var article = db.articles.Where(art => art.id == intid).FirstOrDefault();
                if (article != null)
                {
                    string  
                    html = "<html><head>" +
                    "<meta http-equiv='Content-Type' content='text/html;charset=UTF-8'></head>"
                    + "<body contentEditable='true'>" + article.text + " </body>";
                    webBrowserArticle.NavigateToString(html);
                    
                    var curPAth = System.Reflection.Assembly.GetExecutingAssembly().Location;
                    var path = article.picture;
                    
                    path = path.Replace("/","\\");
                    var filename = System.IO.Path.GetFileName(path);
                    var newPath = 
                        System.IO.Path.Combine(System.IO.Path.GetDirectoryName(curPAth) +"/img/", filename);
                    CurrentAdminPicturePath = newPath;
                   
                    if (File.Exists(newPath))
                    {
                        BitmapImage sharerimg;
                        sharerimg = new BitmapImage(new Uri(newPath));
                        if (article.picture!=newPath)
                        {
                            article.picture = newPath;
                            db.SaveChanges();
                        }

                        imageAdminPicture.Source = sharerimg;

                    }
                    else
                    {
                        
                        try
                        {
                            var path2 = article.picture;
                            var url = "http://hsepress.ru/" + path2;
                            WebClient w = new WebClient();
                            var bytes = w.DownloadData(url);
                            File.WriteAllBytes(newPath, bytes);

                            article.picture = newPath;
                            db.SaveChanges();

                            BitmapImage sharerimg;
                            sharerimg = new BitmapImage(new Uri(newPath));


                            imageAdminPicture.Source = sharerimg;
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message);
                        }
                    }
                    
                    textBoxAdminTitle.Text = article.title;
                    textBoxSubTitle.Text = article.subtitle;
                    
                    var author = db.authors.Where(a => a.authorID == article.author).FirstOrDefault();
                    if (author != null)
                    {
                        textBoxAuthor.Text = author.name + " " + author.surname;
                        CurAuthorId = article.author;
                    }
                    var topic = db.topics.Where(t =>t.topicID  == article.topic).FirstOrDefault();
                    if (topic != null)
                    {
                        textBoxTopic.Text = topic.topicName;
                        CurTopicId = article.topic;
                    }
                    
                    CurrentAdminArticleId = article.id;
                }
            }

        }

        private void buttonback_Click(object sender, RoutedEventArgs e)
        {
            
            searchMode = false;
            artPage = 0;
            gridArticle.Visibility = Visibility.Visible;
            gridAdmin.Visibility = Visibility.Collapsed;
        }

        private void buttonSerialize_Click(object sender, RoutedEventArgs e)
        {
            SaveFileDialog fd = new SaveFileDialog();
            fd.Filter = "xml|*.xml";
            fd.FileOk += Fd_FileOk;
            var res = fd.ShowDialog();
            
        }

        private void Fd_FileOk(object sender, System.ComponentModel.CancelEventArgs e)
        {
          
            SaveFileDialog fd = (SaveFileDialog)sender;
            var file = fd.FileName;
            var ext = System.IO.Path.GetExtension(file);
            if (ext != ".xml") file = file + ".xml";
            using (var db = new TestBaseEntitiesNew())
            {
                var article = db.articles.Where(art => art.id == CurrentAdminArticleId).FirstOrDefault();
                if (article != null)
                {
                    XmlSerializer xsSubmit = new XmlSerializer(typeof(articles));
                    
                    
                    TextWriter tw = new StreamWriter(file);
                    xsSubmit.Serialize(tw, article);
                    
                }
            }
            MessageBox.Show("Сериализовано.");
            Logging.Log("Файл сериализован по пути " + file + ": " + DateTime.Now);
        }

        private void Mouse(int id)
        {
            Article wind = new Article();
            wind.Show();
            using (var db = new TestBaseEntitiesNew())
            {
                var article = db.articles.Where(art => art.id == id).FirstOrDefault();
                wind.name.Content = article.title;
                var author = db.authors.Where(a => a.authorID == article.author).FirstOrDefault();
                wind.author.Content = author.name + " " + author.surname;
                BitmapImage pic;
                pic = new BitmapImage(new Uri(article.picture));
                wind.picture.Source = pic;
                string html = "<html><head>" +
                   "<meta http-equiv='Content-Type' content='text/html;charset=UTF-8'></head>"
                   + "<body contentEditable='true'>" + article.text + " </body>";
                wind.text.NavigateToString(html);
                wind.Title = article.title;
            }
           

        }

       

        private void StackPanel_MouseUp(object sender, MouseButtonEventArgs e)
        {
            StackPanel curr = (StackPanel)sender;
            var tx = (TextBox)curr.Children[2];
            try
            {
                var id = int.Parse(tx.Text);
                Mouse(id);
            }
            catch (Exception ex) {
                Logging.Log("StackPanel_MouseUp;" + ex.Message + ": " + DateTime.Now);
            }
        }

        private void button5_Copy_Click(object sender, RoutedEventArgs e)
        {
            artPage = artPage + 9;
            if (searchMode)
            {
                loadArticles(artPage, artSearch);
            }
            else
            {
                loadArticles(artPage);
            }
        }

        private void button5_Click(object sender, RoutedEventArgs e)
        {
            artPage = artPage - 9;
            if (artPage < 0) artPage = 0;
            if (searchMode)
            {
                loadArticles(artPage, artSearch);
            }
            else
            {
                loadArticles(artPage);
            }
        }

        List<articles> artSearch;
        private void buttonSearch_Click(object sender, RoutedEventArgs e)
        {
            
            artPage = 0;
            using (var db2 = new TestBaseEntitiesNew())
            {

                var artSearched = db2.articles.Where(art=> art.title.ToLower().Contains(textBoxSearch.Text.ToLower()) 
                || art.text.ToLower().Contains(textBoxSearch.Text.ToLower()) ).ToList();
                MessageBox.Show("найдено статей "+artSearched.Count);
                artSearch = artSearched;
                searchMode = true;
                loadArticles(0, artSearched);

            }
        }
        
        private bool searchMode = false;
        private void buttonAllArticles_Click(object sender, RoutedEventArgs e)
        {
            artPage = 0;
            searchMode = false;
            loadArticles(artPage);
        }
    }
}
